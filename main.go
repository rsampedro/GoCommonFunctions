package CommonFunctions

func ContainsInt(object int, list []int) bool {
	for _, element := range list {
		if element == object {
			return true
		}
	}
	return false
}

func ContainsString(object string, list []string) bool {
	for _, element := range list {
		if element == object {
			return true
		}
	}
	return false
}